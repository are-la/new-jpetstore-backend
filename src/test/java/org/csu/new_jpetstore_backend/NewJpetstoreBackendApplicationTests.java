package org.csu.new_jpetstore_backend;

import org.csu.new_jpetstore_backend.common.CommonResponse;
import org.csu.new_jpetstore_backend.entity.Account;
import org.csu.new_jpetstore_backend.entity.Order;
import org.csu.new_jpetstore_backend.persistence.AccountMapper;
//import org.csu.new_jpetstore_backend.service.AccountService;
//import org.csu.new_jpetstore_backend.service.impl.AccountServiceImpl;
import org.csu.new_jpetstore_backend.service.impl.AccountServiceImpl;
import org.csu.new_jpetstore_backend.service.impl.OrderServiceImpl;
import org.junit.jupiter.api.Test;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
@MapperScan(basePackages = "org.csu.new_jpetstore_backend.persistence")
class NewJpetstoreBackendApplicationTests {

    @Autowired
    AccountServiceImpl accountService;

    @Autowired
    OrderServiceImpl orderService;

    @Test
    void contextLoads() {
    }

//    @Test
//    void test(){
//        java.sql.Date date1 = new java.sql.Date(System.currentTimeMillis());
//        System.out.println(date1);
//    }
////
//    @Test
//    void selectUser() {
//        CommonResponse<Object> commonResponse = orderService.viewOrder("888");
//        System.out.println(commonResponse.getData());
//    }
}
