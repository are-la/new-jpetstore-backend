package org.csu.new_jpetstore_backend.controller.front;

import org.csu.new_jpetstore_backend.common.CommonResponse;
import org.csu.new_jpetstore_backend.entity.Product;
import org.csu.new_jpetstore_backend.persistence.ProductMapper;
import org.csu.new_jpetstore_backend.service.ProductService;
import org.csu.new_jpetstore_backend.service.impl.LogServiceImpl;
import org.csu.new_jpetstore_backend.service.impl.ProductServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    ProductServiceImpl productService;
    @Autowired
    LogServiceImpl logService;

    @GetMapping("/{productId}/items")
    @ResponseBody
    public CommonResponse<Object> getItemByProductId(@PathVariable("productId") String productId, HttpServletRequest req){
        String username = (String) req.getAttribute("username");
        String logAction = "browse the product:" + productId;
        logService.addLog(username, logAction);
        System.out.println(productId);
        CommonResponse<Object> response = productService.getItemByProductId(productId);
        return response;
    }

    @GetMapping("/{name}")
    @ResponseBody
    public CommonResponse<Object> SearchByProductName(@PathVariable("name") String name){
        System.out.println(name);
        CommonResponse<Object> response = productService.SearchByProductName(name);
        return response;
    }

    @GetMapping("/products")
    @ResponseBody
    public CommonResponse<Object> GetAllProducts(){
        CommonResponse<Object> response = productService.GetAllProducts();
        return response;
    }
    @GetMapping("products/{key}")
    @ResponseBody
    public CommonResponse<List<Product>> searchProducts(@PathVariable("key") String keyword){
        return productService.searchProducts(keyword);
    }
}
