package org.csu.new_jpetstore_backend.controller.front;

import org.csu.new_jpetstore_backend.common.CommonResponse;
import org.csu.new_jpetstore_backend.entity.Item;
import org.csu.new_jpetstore_backend.service.impl.ItemServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/item")
public class ItemController {

    @Autowired
    ItemServiceImpl itemService;

     @GetMapping("/items")
    public CommonResponse<Object> getAllItems(){
         CommonResponse<Object> response = itemService.GetAllItems();
         return response;
     }
     @PostMapping("/getItem/{itemId}")
    public  CommonResponse<Item> getItemById(@PathVariable("itemId") String itemId){
         CommonResponse<Item> commonResponse=itemService.getItemById(itemId);
         return  commonResponse;
     }
}
