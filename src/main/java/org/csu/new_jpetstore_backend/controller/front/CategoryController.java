package org.csu.new_jpetstore_backend.controller.front;

import org.csu.new_jpetstore_backend.common.CommonResponse;
import org.csu.new_jpetstore_backend.entity.Category;
import org.csu.new_jpetstore_backend.service.impl.CategoryServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    CategoryServiceImpl categoryService;

    @GetMapping(value = "/categories")
    public CommonResponse<Object> getCategoryList(){
        CommonResponse<Object> response = categoryService.getCategories();
        return response;
    }

    @GetMapping(value = "/categories/{catid}/products")
    @ResponseBody
    public CommonResponse<Object> getProductListByCategoryId(@PathVariable("catid") String categoryId){
        System.out.println(categoryId);
        CommonResponse<Object> response = categoryService.getProductListByCategoryId(categoryId);
        return response;
    }
}
