package org.csu.new_jpetstore_backend.controller.front;

import com.alipay.api.AlipayApiException;
import org.csu.new_jpetstore_backend.common.CommonResponse;
import org.csu.new_jpetstore_backend.common.LoginResponse;
import org.csu.new_jpetstore_backend.entity.Account;
import org.csu.new_jpetstore_backend.entity.AliPayBean;
//import org.csu.new_jpetstore_backend.entity.Order;
import org.csu.new_jpetstore_backend.entity.Order;
import org.csu.new_jpetstore_backend.service.impl.LogServiceImpl;
import org.csu.new_jpetstore_backend.service.PayService;
import org.csu.new_jpetstore_backend.service.impl.AccountServiceImpl;
import org.csu.new_jpetstore_backend.service.impl.OrderServiceImpl;
import org.csu.new_jpetstore_backend.service.impl.PayServiceImpl;
import org.csu.new_jpetstore_backend.vo.CartVO;
import org.csu.new_jpetstore_backend.vo.OrderVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/order")
@CrossOrigin
public class OrderController {
    @Autowired
    OrderServiceImpl orderService;

    @Autowired
    LogServiceImpl logService;

    /**日志对象*/
    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);

    @PostMapping(value = "/addOrder")
    public CommonResponse<Object> addOrder(@RequestBody Order order, HttpServletRequest req){
        String username = (String) req.getAttribute("username");
        String logAction = "place an order" + order.getOrderid();
        logService.addLog(username,logAction);
//        System.out.println(order.getUsername()+"     "+order.getOrderid()+"     "+order.getAddress()+"     "+order.getTotalPrice());
        CommonResponse<Object> response = orderService.addOrder(order, username);

        return response;
    }

    @PostMapping("{username}/viewOrder")
    public Object viewOrder(@PathVariable("username") String username)
    {
        System.out.println("viewOrder传入的username为"+username);

        CommonResponse<Object> commonResponse =orderService.viewOrder(username);
        return commonResponse;
    }
    @GetMapping("/VisualOrderVO/{username}")
    public  CommonResponse<List<OrderVO>> VisualOrderVO(@PathVariable("username") String username){
        return orderService.getOrderVoList(username);
    }

}
