package org.csu.new_jpetstore_backend.common;

import org.csu.new_jpetstore_backend.entity.Account;
import org.springframework.http.HttpStatus;

public class LoginResponse {
    private String message;
    private String token;
    private HttpStatus httpStatus;
    private Account account;

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }
}
