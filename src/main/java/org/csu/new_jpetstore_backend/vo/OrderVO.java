package org.csu.new_jpetstore_backend.vo;

import java.math.BigDecimal;
import java.util.Date;

public class OrderVO {
    private Date orderDate;

    private String totalPrice;

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String  totalPrice) {
        this.totalPrice = totalPrice;
    }
}
