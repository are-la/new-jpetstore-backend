package org.csu.new_jpetstore_backend.vo;
import lombok.Data;

@Data
public class CartVO {

    private int id;
    private String username;
    private String itemId;
    private int quantity;

    private float price;
}