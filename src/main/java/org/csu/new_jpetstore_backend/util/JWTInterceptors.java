package org.csu.new_jpetstore_backend.util;

import com.auth0.jwt.exceptions.AlgorithmMismatchException;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.csu.new_jpetstore_backend.entity.Account;
import org.csu.new_jpetstore_backend.service.impl.AccountServiceImpl;
import org.csu.new_jpetstore_backend.service.impl.ItemServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.security.SignatureException;
import org.csu.new_jpetstore_backend.util.JWTUtils;
//@ComponentScan("org.csu.new_jpetstore_backend.service")
//@Configurable
public class JWTInterceptors implements HandlerInterceptor {

    @Autowired
    AccountServiceImpl accountService;

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object object) throws Exception {
        // 从请求头中取出 token  这里需要和前端约定好把jwt放到请求头一个叫token的地方
        String token = httpServletRequest.getHeader("Authorization");
        // 如果不是映射到方法直接通过
        if (!(object instanceof HandlerMethod)) {
            return true;
        }
        HandlerMethod handlerMethod = (HandlerMethod) object;
        Method method = handlerMethod.getMethod();
//        System.out.println(object.toString());
        //检查是否有passToken注释，有则跳过认证

//            System.out.println("被jwt拦截需要验证");
            // 执行认证
            if (token == null) {
                System.out.println("token null");
                //这里其实是登录失效,没token了   这个错误也是我自定义的，读者需要自己修改
                throw new SignatureException("token error");
            }

            // 获取 token 中的 user Name
            String username = JWTUtils.getAudience(token);

            //找找看是否有这个user   因为我们需要检查用户是否存在，读者可以自行修改逻辑
        System.out.println(username);
//        System.out.println("aaa");
//        System.out.println(accountService);

        Account user = accountService.selectAccountByUsername(username);
//        System.out.println(user);
            if (user == null) {
                System.out.println("user null");
                //这个错误也是我自定义的
                throw new Exception();
            }

            // 验证 token
            JWTUtils.verifyToken(token, username);

//            //获取载荷内容
//            String userName = JwtUtils.getClaimByName(token, "userName").asString();
//            String realName = JwtUtils.getClaimByName(token, "realName").asString();
//
//            //放入attribute以便后面调用
            httpServletRequest.setAttribute("username", username);
//            request.setAttribute("realName", realName);


            return true;

    }
}
