package org.csu.new_jpetstore_backend.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.csu.new_jpetstore_backend.exception.JWTException;
import org.springframework.http.HttpStatus;

import javax.xml.stream.XMLStreamReader;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

public class JWTUtils {

    /**
     * 生成token  header.payload.singature
     */
    /**
     签发对象：这个用户的id
     签发时间：现在
     有效时间：300分钟
     载荷内容：暂时设计为：这个人的名字，这个人的昵称
     加密密钥：这个人的id加上一串字符串
     */
    public static String createToken(String username, String email) {

        Calendar nowTime = Calendar.getInstance();
        nowTime.add(Calendar.MINUTE,300);
        Date expiresDate = nowTime.getTime();

        return JWT.create().withAudience(username)   //签发对象
                .withIssuedAt(new Date())    //发行时间
                .withExpiresAt(expiresDate)  //有效时间
                .withClaim("email", email)    //载荷，随便写几个都可以
//                .withClaim("realName", realName)
                .sign(Algorithm.HMAC256(username+"HelloLehr"));   //加密
    }

    /**
     * 检验合法性，其中secret参数就应该传入的是用户的id
     * @param token
     * @throws
     */
    public static void verifyToken(String token, String secret) throws Exception {
        DecodedJWT jwt = null;
        try {
            JWTVerifier verifier = JWT.require(Algorithm.HMAC256(secret+"HelloLehr")).build();
            jwt = verifier.verify(token);
        } catch (Exception e) {
            //效验失败
            //这里抛出的异常是我自定义的一个异常，你也可以写成别的
            throw new JWTException("Invalid JWT", HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * 获取签发对象
     */
    public static String getAudience(String token) throws Exception {
        String audience = null;
        try {
            audience = JWT.decode(token).getAudience().get(0);
        } catch (JWTDecodeException j) {
            //这里是token解析失败
            throw new JWTDecodeException("获取签发对象失败");
        }
        return audience;
    }


    /**
     * 通过载荷名字获取载荷的值
     */
    public static Claim getClaimByName(String token, String name){
        return JWT.decode(token).getClaim(name);
    }
}