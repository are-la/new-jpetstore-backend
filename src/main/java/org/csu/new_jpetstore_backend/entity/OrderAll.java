package org.csu.new_jpetstore_backend.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName("orders")
public class OrderAll {

    @TableId(type = IdType.INPUT)
    private String orderid;

    @TableField(value = "userid")
    private String username;

    @TableField(value = "name")
    private String name;

    @TableField(value = "telephone")
    private String telephone;

    @TableField(value = "orderdate")
    private Date orderDate;

    @TableField(value = "shipaddr1")
    private String shipaddress;

    @TableField(value = "billaddr1")
    private String billaddress;

    @TableField(value = "totalprice")
    private String totalPrice;

    @TableField(value = "creditcard")
    private String creditcard;

    @TableField(value = "cardtype")
    private String cardtype;

    @TableField(value = "billtofirstname")
    private String firstname;

    @TableField(value = "billtolastname")
    private String lastname;

    @TableField(value = "shipcity")
    private String city;

//    @TableField(value = "shipstate")
//    private String state;

    @TableField(value = "shipzip")
    private String shipzip;

    @TableField(value = "shipcountry")
    private String country;




    public OrderAll(Order order, Account account)
    {
        this.orderid = order.getOrderid();
        this.username = account.getUsername();
        this.shipaddress = order.getShipaddress();
        this.billaddress = order.getBilladdress();
        this.totalPrice = order.getTotalPrice();
        this.orderDate=order.getOrderDate();
        this.cardtype = order.getCardtype();
        this.name = order.getName();
        this.telephone = order.getTelephone();
        this.creditcard = order.getCreditcard();
        this.firstname = account.getFirstname();
        this.lastname = account.getLastname();
        this.city = account.getCity();
//        this.state = account.getState();
        this.shipzip = order.getShipzip();
        this.country = account.getCountry();
    }

    public OrderAll(){};


}
