package org.csu.new_jpetstore_backend.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName("orders")
public class Order {
//    @TableId(type = IdType.INPUT)
    private String orderid;

//    @TableField(value = "userid")
    private String username;

    private Date orderDate;

//    @TableField(value = "shipaddr1")
    private String shipaddress;
    private String billaddress;
    private String shipzip;
    private String name;
    private String telephone;

//    @TableField(value = "totalprice")
    private String totalPrice;

    private String creditcard;

    private String cardtype;

    public Order(){}

    public Order(String id, String username, String address){
        this.orderid = id;
        this.username = username;
        this.shipaddress = address;
    }

}
