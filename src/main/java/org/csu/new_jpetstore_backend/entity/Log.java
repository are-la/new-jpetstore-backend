package org.csu.new_jpetstore_backend.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@TableName("log")
@Component
public class Log {
    @TableId(value = "id" ,type= IdType.INPUT)
    private int id;
    @TableField(value = "userid")
    private String userid;
    private String date;
    private String action;

}
