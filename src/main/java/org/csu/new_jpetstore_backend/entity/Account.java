package org.csu.new_jpetstore_backend.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("account")
public class Account {
    @TableId(value = "username", type = IdType.INPUT)
    private String username;
    private String password;
    private String email;
    private String firstname;
    private String lastname;
    private String status;
    private String phone;
    @TableField(value = "addr1")
    private String address1;
    @TableField(value = "addr2")
    private String address2;
    private String city;
    private String state;
    private String zip;
    private String country;
    private String role;

    public Account(){}

    public Account(String username,String password, String email, String firstname, String lastname, String status, String phone, String address1, String address2, String city, String state, String zip, String country,String role){
        this.username = username;
        this.password = password;
        this.email = email;
        this.firstname = firstname;
        this.lastname = lastname;
        this.status = status;
        this.phone = phone;
        this.address1 = address1;
        this.address2 = address2;
        this.city = city;
        this.state = state;
        this.zip = zip;
        this.country = country;
        this.role = role;
    }
}
