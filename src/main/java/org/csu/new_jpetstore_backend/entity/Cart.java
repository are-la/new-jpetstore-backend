package org.csu.new_jpetstore_backend.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("cart")
public class Cart {
    @TableId(type = IdType.INPUT)
    private int id;

    @TableField(value = "userid")
    private String username;

    @TableField(value = "itemid")
    private String itemId;

    private int quantity;

    public Cart(){}

    public Cart(int id, String username, String itemId, int quantity){
        this.id = id;
        this.username = username;
        this.itemId = itemId;
        this.quantity = quantity;
    }
}
