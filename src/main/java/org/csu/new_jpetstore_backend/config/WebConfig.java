package org.csu.new_jpetstore_backend.config;

import org.csu.new_jpetstore_backend.util.JWTInterceptors;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
public class WebConfig implements WebMvcConfigurer {
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**");
    }

    @Bean
    public JWTInterceptors jwtInterceptors() {
        return new JWTInterceptors();
    }
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(jwtInterceptors())
                .addPathPatterns("/**")  // 其他接口token验证
                .excludePathPatterns("/account/login", "/account/verification/{email}", "/account/register", "/account/checkusername", "/account/checkemail", "/account/checkphone");  // 所有用户都放行
    }
}
