package org.csu.new_jpetstore_backend.config;

import com.alibaba.fastjson.JSON;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import org.csu.new_jpetstore_backend.entity.AliPayBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

public class Alipay {
    /**日志对象*/
    private static final Logger logger = LoggerFactory.getLogger(Alipay.class);

    private  final String format = "json";

    /**
     * appId
     */

    private  String appId="2021000119641673";

    /**
     * 商户私钥
     */

    private  String privateKey="MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCW0rm3IV9pqVcRggpZXiEBg84MMc4A84McWVKNvZP1RY32yqDepVhcaR1Kqv0deo/7RfHdHNRhv0ebHfEko/iKeCTexDrayeq3MLnzPb/384ndFyBueyD2lCNwaTrY9HM9ZebqKcT3wg2ICY9p5Zr4M7fsO8nFPSYx9as4IysvGhhiYUbiyVXavbznMO2LlEZRwoDt8kUZVpi1pZGBywc0Fm0b6s6ejlCEg0Zv7WYdH0vDZbTHPzU08dK5GL0pyYtBXWoegt7f889fh32V/AemSZ8nI04hfojXsnteAjyfXhZYXzhIuOAtsFvt0g5dvPjUJEL7MHXh7mvziaJSm2w3AgMBAAECggEAC5GgYzaxHea6YgJqjs5Uk8wrau4LEjGblfoYBf1Lsx2htRN34XwnWfdKIucYwUEceDa2RbKna9QDRxe13oyhTOXIVjoRFaU1VtXq/Yh4CCnrIkVa3H6fWaqlSUMZG3ksucHuQ3nuvN5qsUuib4oIKA+3hOxVHscA/gYNGyI/dr5YhGTHPxruiR8fHKwzdR7iqGTpnlyW8VMSsiREdy76zrUUNlQGfq0Pr/9jLEVGI1CrAR+2ic0DkbyF66hbeui9GBVMX4tgJTUXkAKkvOy8r7GMiqhVEX3boV8HiDszlUqxC8zoRHvHIKB+blgZeYyd/9J1HywO+Z/88I6ZdTAI6QKBgQDLtI+YONyHQuQO8PueXYAQBORBu+XVYFgOdV/zBwxQfprO4Zn6obn4qUjtVUxLWrBPl4xxVmJupfZPLyi0KCe9tPDhvnRlTKa/N4gpU4OJ8yDw/IUeCXRwC1/LXMucjJDvbI96jpGUJRw5Frap8GNeBMMfSCv4bPaNzVP/llQNwwKBgQC9isSC0sH/U1+mvaphFtb16rTlJWiYXXs0EzSHPBWkpX+LHDT7KgXF2fNzp0LsY0O7wwIsZYdoDe8N/qTC0XIjD4kuJOT8oG12j6uSALPJm2p2pc/ajII5BaGGO6NPBtHr+dF4F8BYsXcbG6ACnju8F7h6i5en5MAghxej3d08fQKBgQCC6pVKqGiaiZPWQmwDuGWgZfRohqDMRHHzQN4ca5Avf15wzS7Pk/bYZ7uVxDcXSr57OJOrF3CFWiX1eAJ53kDEPDB/dI6Lt/qGc+MPZDvqgZXPKwtGZZ8ujzDOmuHEx9Yx+ZwwkMXg5wS/b+iPSQ4jk33vccvIigbe1XJBldSJTwKBgQCZTnRDnFRtP/JN23rDrMWRgpVuAiPnmTTdmesStvaKhZHz4oy32fVxYigXk2H98QwIAmLvpe8vqpWArEZ3jRDUaux59j2AWAIxs2MCZK8Gkj46WUMVcjCiDnvnJrWgZdPFGfrjN9LL6j58Hov9kh6etFDyTZYMpmj/5kSotfsLwQKBgErDXduXWcKDE4qJ3mGyFV3wGf++/6iiBZ5ry3T3yYkCz45Fkqy9tAuwJy+nJ5ivyBza6TQ/4MsYsTAxVmz8U+MhzLfBExNJcL8JTDhUQnAW7bhvWvGbNBVCDHdH3R78EQX75faLn5KL8RK1ltlqpJ4AyaP/eIzMBnrPWLllT3by";

    /**
     * 支付宝公钥
     */

    private  String publicKey="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkKn0B2BMuu7U/kmAhSXwKSQs8oMMs/6mG+JZiL/xmTcxYmc15yqEhHvoSwkkeasrzb/0/2uq5cyGxFcBuozZqNx4p1HsCmDn/Tyf7GfQbIy6rz6KVzxxWWrZpjmZLk1qzJS1mcMdU11d+EoVXQzfqDjVIi1fQYuvjF57b4t6xrXiVb11LfMCkUFV5jewJzDZ7ulIAZ1o2h5066HnibhtMhmHdkRWyb/rNJwkln1mQzo42oo754aSCXyGYzp825f4HsYExjdsONNiNnzOK3VInQSYnvj11FjEXk2ijf/xKweRCN5a9c2sjh//h0eohSJqOokJBlj9ZWyPdXwlIQ2T3wIDAQAB";

    /**
     * 服务器异步通知页面路径，需要公网能访问到
     */

    private  String notifyUrl="http://localhost:3000/paySuccess";
   // private  String notifyUrl="www.baidu.com";
    /**
     * 服务器同步通知页面路径，需要公网能访问到
     */

    private  String returnUrl="http://localhost:3000/paySuccess";
   // private  String returnUrl="www.baidu.com";


    /**
     * 签名方式
     */

    private  String signType="RSA2";

    public String getAppId() {
        return appId;
    }



    /**
     * 字符编码格式
     */

    private  String charset="utf-8";

    /**
     * 支付宝网关
     */
  
    private  String gatewayUrl="https://openapi.alipaydev.com/gateway.do";

    public String pay(AliPayBean aliPayBean) throws AlipayApiException {

        AlipayClient alipayClient = new DefaultAlipayClient(
                gatewayUrl, appId, privateKey, format, charset, publicKey, signType);

        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        alipayRequest.setReturnUrl(returnUrl);
        alipayRequest.setNotifyUrl(notifyUrl);
        alipayRequest.setBizContent(JSON.toJSONString(aliPayBean));
        logger.info("封装请求支付宝付款参数为:{}", JSON.toJSONString(alipayRequest));

        String result = alipayClient.pageExecute(alipayRequest).getBody();
        logger.info("请求支付宝付款返回参数为:{}", result);

        return result;
    }
}

