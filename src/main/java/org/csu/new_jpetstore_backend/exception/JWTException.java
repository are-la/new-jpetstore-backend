package org.csu.new_jpetstore_backend.exception;

import org.springframework.http.HttpStatus;

public class JWTException extends Exception {
    private HttpStatus httpStatus;
//    private String message;

    public JWTException(String message, HttpStatus httpStatus) {
        super(message);
        this.httpStatus = httpStatus;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }
}
