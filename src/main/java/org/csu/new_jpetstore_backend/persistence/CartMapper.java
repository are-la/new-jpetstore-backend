package org.csu.new_jpetstore_backend.persistence;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.csu.new_jpetstore_backend.entity.Cart;
import org.springframework.stereotype.Repository;

@Repository
public interface CartMapper extends BaseMapper<Cart> {
}
