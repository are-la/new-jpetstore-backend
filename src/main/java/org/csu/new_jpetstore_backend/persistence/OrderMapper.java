package org.csu.new_jpetstore_backend.persistence;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.csu.new_jpetstore_backend.entity.OrderAll;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderMapper extends BaseMapper<OrderAll> {
}
