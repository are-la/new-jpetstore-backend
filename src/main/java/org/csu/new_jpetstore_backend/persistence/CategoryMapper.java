package org.csu.new_jpetstore_backend.persistence;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.csu.new_jpetstore_backend.entity.Category;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryMapper extends BaseMapper<Category> {
}
