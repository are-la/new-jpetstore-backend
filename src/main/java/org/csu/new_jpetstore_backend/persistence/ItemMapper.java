package org.csu.new_jpetstore_backend.persistence;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.csu.new_jpetstore_backend.entity.Item;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemMapper extends BaseMapper<Item> {
}
