package org.csu.new_jpetstore_backend.service;

import org.csu.new_jpetstore_backend.common.CommonResponse;
import org.csu.new_jpetstore_backend.entity.Account;
import org.csu.new_jpetstore_backend.entity.Order;
import org.csu.new_jpetstore_backend.vo.CartVO;
import org.csu.new_jpetstore_backend.vo.OrderVO;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public interface OrderService {
    CommonResponse <Object> addOrder(Order order, String username);

    CommonResponse <Object> viewOrder(String username);
    //用于数据可视化
    CommonResponse<List<OrderVO>> getOrderVoList(String username);
}
