package org.csu.new_jpetstore_backend.service;

import org.csu.new_jpetstore_backend.common.CommonResponse;
import org.csu.new_jpetstore_backend.entity.Category;
import org.springframework.web.bind.annotation.PathVariable;

public interface CategoryService {
    CommonResponse<Object> getCategories();
    CommonResponse<Object> getProductListByCategoryId(String categoryId);
}
