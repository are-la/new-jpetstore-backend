package org.csu.new_jpetstore_backend.service;

import com.alipay.api.AlipayApiException;
import org.csu.new_jpetstore_backend.entity.AliPayBean;
import org.springframework.stereotype.Service;


public interface PayService {
    String aliPay(AliPayBean aliPayBean) throws AlipayApiException;

}