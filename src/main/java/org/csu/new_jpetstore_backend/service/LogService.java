package org.csu.new_jpetstore_backend.service;

import org.csu.new_jpetstore_backend.common.CommonResponse;
import org.csu.new_jpetstore_backend.entity.Log;

import java.util.List;

public interface LogService {
    void addLog(String userid,String action);
    CommonResponse<List<Log>> getTricks(String userid);
}
