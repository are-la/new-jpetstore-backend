package org.csu.new_jpetstore_backend.service;

import org.csu.new_jpetstore_backend.common.CommonResponse;
import org.csu.new_jpetstore_backend.entity.Product;

import java.util.List;

public interface ProductService {
    CommonResponse<Object> getItemByProductId(String productId);
    CommonResponse<Object> SearchByProductName(String name);
    CommonResponse<Object> GetAllProducts();
    CommonResponse<List<Product>> searchProducts(String keyword);
}
