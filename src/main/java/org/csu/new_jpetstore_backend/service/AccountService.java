package org.csu.new_jpetstore_backend.service;

import org.csu.new_jpetstore_backend.common.CommonResponse;
import org.csu.new_jpetstore_backend.entity.Account;
import org.csu.new_jpetstore_backend.vo.CartVO;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public interface AccountService {
    Account selectAccountByUsername(String username);

    CommonResponse<Object> register(Account account);
    CommonResponse<Object> checkEmail(String email);
    CommonResponse<Object> checkUsername(String username);
    CommonResponse<Object> checkPhone(String Phone);
    CommonResponse<Object> updateAccountInformation(Account account);
    CommonResponse<List<CartVO>> getCartByAccount(@PathVariable("id") String username);

    CommonResponse<List<CartVO>> updateCart(@PathVariable("userid") String username, @PathVariable("id") String itemId, @RequestParam String quantity);
    CommonResponse<List<CartVO>> addToCart(@PathVariable("userid") String username, @PathVariable("id") String itemId);

}
