package org.csu.new_jpetstore_backend.service;

import org.csu.new_jpetstore_backend.common.CommonResponse;
import org.csu.new_jpetstore_backend.entity.Item;

public interface ItemService {
    CommonResponse<Object> GetAllItems();
    CommonResponse<Item> getItemById(String itemId);
}
