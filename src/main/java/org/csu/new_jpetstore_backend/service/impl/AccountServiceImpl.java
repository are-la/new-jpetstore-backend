package org.csu.new_jpetstore_backend.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import org.csu.new_jpetstore_backend.common.CommonResponse;
import org.csu.new_jpetstore_backend.entity.Account;
import org.csu.new_jpetstore_backend.entity.Cart;
import org.csu.new_jpetstore_backend.entity.Item;
import org.csu.new_jpetstore_backend.persistence.AccountMapper;
import org.csu.new_jpetstore_backend.persistence.CartMapper;
import org.csu.new_jpetstore_backend.persistence.ItemMapper;
import org.csu.new_jpetstore_backend.service.AccountService;
import org.csu.new_jpetstore_backend.vo.CartVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Service("AccountService")
public class AccountServiceImpl implements AccountService {

    @Autowired
    AccountMapper accountMapper;

    @Autowired
    CartMapper cartMapper;

    @Autowired
    ItemMapper itemMapper;

    @Override
    public Account selectAccountByUsername(String username) {
        System.out.println("account service");
        System.out.println(username);
        return accountMapper.selectById(username);
    }

    @Override
    public CommonResponse<Object> register(Account account) {
        int recordNum = accountMapper.insert(account);
        if (recordNum > 0) {
            return CommonResponse.createForSuccessMessage("register success!");
        } else {
            return CommonResponse.createForError("register fail!");
        }
    }

    @Override
    public CommonResponse<Object> checkEmail(String email) {
        QueryWrapper<Account> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("email", email);
        List<Account> account = accountMapper.selectList(queryWrapper);
        System.out.println(account);
        if (account.size() == 0) {
            return CommonResponse.createForSuccessMessage("Email is available!");
        } else {
            return CommonResponse.createForError("Email is unavailable!");
        }
    }

    @Override
    public CommonResponse<Object> checkUsername(String username) {
        Account account = accountMapper.selectById(username);
        if (account == null) {
            return CommonResponse.createForSuccessMessage("username is available!");
        } else {
            return CommonResponse.createForError("username is unavailable!");
        }
    }

    @Override
    public CommonResponse<Object> checkPhone(String phone) {
        QueryWrapper<Account> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("phone", phone);
        List<Account> account = accountMapper.selectList(queryWrapper);
        System.out.println(account);
        if (account.size() == 0) {
            return CommonResponse.createForSuccessMessage("phone is available!");
        } else {
            return CommonResponse.createForError("phone is unavailable!");
        }
    }

    @Override
    public CommonResponse<Object> updateAccountInformation(Account account) {
        UpdateWrapper<Account> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("username", account.getUsername());
        int isUpdateSuccess = accountMapper.update(account, updateWrapper);
        if (isUpdateSuccess == 0) {
            return CommonResponse.createForError("update fail!");
        }
        return CommonResponse.createForSuccess("update success!", account);
    }

    @Override
    public CommonResponse<List<CartVO>> getCartByAccount(@PathVariable("id") String username) {
        QueryWrapper<Cart> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("userid", username);

        List<Cart> cartList = cartMapper.selectList(queryWrapper);

        List<CartVO> cartVOList = new ArrayList<>();

        for (int i = 0; i < cartList.size(); i++) {
            cartVOList.add(cartToCartVO(cartList.get(i)));
        }

        if (cartList.isEmpty()) {
            return CommonResponse.createForSuccess("您的购物车暂时为空！快去添加商品吧~", cartVOList);
        }
        return CommonResponse.createForSuccess(cartVOList);
    }

    @Override
    public CommonResponse<List<CartVO>> updateCart(@PathVariable("userid") String username, @PathVariable("id") String itemId, @RequestParam String quantity) {
        QueryWrapper<Cart> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("userid", username);
        queryWrapper.eq("itemid", itemId);

        Cart cart = cartMapper.selectOne(queryWrapper);
        QueryWrapper<Cart> queryWrapper2 = new QueryWrapper<>();
        List<Cart> cartList0 = cartMapper.selectList(queryWrapper2);

        if (cart == null) {
            Cart newCart = new Cart(cartList0.size() + 1, username, itemId, 1);
            cart = newCart;
            cartMapper.insert(cart);
        }

        if (Integer.parseInt(quantity) == 0) {
            cartMapper.delete(queryWrapper);
        } else {
            cart.setQuantity(Integer.parseInt(quantity));
            cartMapper.updateById(cart);
        }

        QueryWrapper<Cart> queryWrapper1 = new QueryWrapper<>();
        queryWrapper1.eq("userid", username);
        List<Cart> cartList = cartMapper.selectList(queryWrapper1);
        List<CartVO> cartVOList = new ArrayList<>();

        for (int i = 0; i < cartList.size(); i++) {
            cartVOList.add(cartToCartVO(cartList.get(i)));
        }
        return CommonResponse.createForSuccess(cartVOList);
    }

    private CartVO cartToCartVO(Cart cart) {

        CartVO cartVO = new CartVO();
        cartVO.setUsername(cart.getUsername());
        cartVO.setQuantity(cart.getQuantity());
        cartVO.setId(cart.getId());
        cartVO.setItemId(cart.getItemId());

        Item item = itemMapper.selectById(cart.getItemId());
//        System.out.println(cart.getQuantity());
        cartVO.setPrice(item.getListPrice());
        return cartVO;
    }

    @Override
    public CommonResponse<List<CartVO>> addToCart(@PathVariable("userid") String username, @PathVariable("id") String itemId) {
        QueryWrapper<Cart> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("userid", username);
        queryWrapper.eq("itemid", itemId);

        Cart cart = cartMapper.selectOne(queryWrapper);
        QueryWrapper<Cart> queryWrapper2 = new QueryWrapper<>();
        List<Cart> cartList0 = cartMapper.selectList(queryWrapper2);

        if (cart == null) {
            Cart newCart = new Cart(cartList0.size() + 1, username, itemId, 1);
            cart = newCart;
            cartMapper.insert(cart);
        } else {
            cart.setQuantity(cart.getQuantity() + 1);
            cartMapper.updateById(cart);
        }

        QueryWrapper<Cart> queryWrapper1 = new QueryWrapper<>();
        queryWrapper1.eq("userid", username);
        List<Cart> cartList = cartMapper.selectList(queryWrapper1);

        List<CartVO> cartVOList = new ArrayList<>();

        for (int i = 0; i < cartList.size(); i++) {

            cartVOList.add(cartToCartVO(cartList.get(i)));
        }

        return CommonResponse.createForSuccess(cartVOList);
    }

}
