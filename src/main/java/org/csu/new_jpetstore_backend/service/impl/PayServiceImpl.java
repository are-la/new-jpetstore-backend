package org.csu.new_jpetstore_backend.service.impl;


import com.alipay.api.AlipayApiException;
import org.csu.new_jpetstore_backend.config.Alipay;
import org.csu.new_jpetstore_backend.entity.AliPayBean;
import org.csu.new_jpetstore_backend.service.PayService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


public class PayServiceImpl implements PayService {
    /**日志对象*/
    private static final Logger logger = LoggerFactory.getLogger(PayServiceImpl.class);


    private Alipay alipay;


    @Override
    public String aliPay(AliPayBean aliPayBean) throws AlipayApiException {
        logger.info("调用支付服务接口...");
        alipay=new Alipay();
        System.out.println(alipay.getAppId());
        return alipay.pay(aliPayBean);
    }
}