package org.csu.new_jpetstore_backend.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.csu.new_jpetstore_backend.common.CommonResponse;
import org.csu.new_jpetstore_backend.entity.Category;
import org.csu.new_jpetstore_backend.entity.Product;
import org.csu.new_jpetstore_backend.persistence.CategoryMapper;
import org.csu.new_jpetstore_backend.persistence.ProductMapper;
import org.csu.new_jpetstore_backend.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Service("CategoryService")
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    CategoryMapper categoryMapper;
    @Autowired
    ProductMapper productMapper;

    @Override
    public CommonResponse<Object> getCategories() {
        List<Category> categoryList = categoryMapper.selectList(null);
        if(categoryList.isEmpty()){
            return CommonResponse.createForError("have not categories");
        }
        return CommonResponse.createForSuccess("getCategories success!", categoryList);
    }

    @Override
    public CommonResponse<Object> getProductListByCategoryId(String categoryId) {
        QueryWrapper<Product> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("category", categoryId);

        List<Product> productList = productMapper.selectList(queryWrapper);
        if(productList.isEmpty()){
            return CommonResponse.createForSuccessMessage("have not such products!");
        }
        return CommonResponse.createForSuccess("getProducts success!", productList);
    }
}
