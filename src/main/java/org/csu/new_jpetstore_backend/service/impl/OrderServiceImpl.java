package org.csu.new_jpetstore_backend.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.csu.new_jpetstore_backend.common.CommonResponse;
import org.csu.new_jpetstore_backend.entity.Account;
import org.csu.new_jpetstore_backend.entity.Cart;
import org.csu.new_jpetstore_backend.entity.Order;
import org.csu.new_jpetstore_backend.persistence.AccountMapper;
import org.csu.new_jpetstore_backend.persistence.CartMapper;
import org.csu.new_jpetstore_backend.persistence.OrderMapper;
import org.csu.new_jpetstore_backend.service.OrderService;
import org.csu.new_jpetstore_backend.entity.OrderAll;
import org.csu.new_jpetstore_backend.vo.OrderVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service("OrderService")
public class OrderServiceImpl implements OrderService {
    @Autowired
    OrderMapper orderMapper;


    @Autowired
    CartMapper cartMapper;

    @Autowired
    AccountMapper accountMapper;


    @Override
    public CommonResponse<Object> addOrder(Order order, String username) {
        java.sql.Date date = new java.sql.Date(System.currentTimeMillis());
        order.setOrderDate(date);

        Account account = accountMapper.selectById(username);
        OrderAll orderAll = new OrderAll(order,account);

        int recordNum = orderMapper.insert(orderAll);

        if(recordNum > 0){
            QueryWrapper<Cart> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("userid", order.getUsername());
            cartMapper.delete(queryWrapper);

            return  CommonResponse.createForSuccessMessage("addOrder success!");
        }else {
            return  CommonResponse.createForError("addOrder fail!");
        }
    }

    @Override
    public CommonResponse<Object> viewOrder(String username)
    {
        QueryWrapper<OrderAll> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("userid", username);
        List<OrderAll> orderAllList = orderMapper.selectList(queryWrapper);

        return CommonResponse.createForSuccess("获取所有order成功", orderAllList);
    }

//可视化数据
    @Override
    public CommonResponse<List<OrderVO>> getOrderVoList(String username) {
        QueryWrapper<OrderAll> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("userid", username);
        List<OrderAll> orderList = orderMapper.selectList(queryWrapper);
        List<OrderVO> orderVOList=new ArrayList<OrderVO>();
        /*for (int i=0;i<orderList.size();i++){
            OrderVO orderVO=new OrderVO();
            orderVO.setOrderDate(orderList.get(i).getOrderDate());
            orderVO.setTotalPrice(orderList.get(i).getTotalPrice());
            orderVOList.add(orderVO);
        }*/

       OrderVO orderVO=new OrderVO();
        orderVO.setOrderDate(orderList.get(0).getOrderDate());
        orderVO.setTotalPrice(orderList.get(0).getTotalPrice());
        orderVOList.add(orderVO);

        for(int i=1;i<orderList.size();i++){
            int k=0;
            Date currentDate=orderList.get(i-1).getOrderDate();
            if(!(orderList.get(i).getOrderDate().equals(currentDate))){
                OrderVO orderSales1=new OrderVO();
                orderSales1.setOrderDate(orderList.get(i).getOrderDate());
                orderSales1.setTotalPrice(orderList.get(i).getTotalPrice());
                orderVOList.add(orderSales1);
            }
            
        }




      /*  List<OrderSales> orderSalesList=new ArrayList<OrderSales>();
        List<Order> orderList=orderMapper.getAllOrders();
        OrderSales orderSales=new OrderSales();
        orderSales.setOrderDate(orderList.get(0).getOrderDate());
        orderSales.setOrderSales(orderList.get(0).getTotalPrice());
        orderSalesList.add(orderSales);
        for(int i=1;i<orderList.size();i++){
            int k=0;
            Date currentDate=orderList.get(i-1).getOrderDate();
            if(!(orderList.get(i).getOrderDate().equals(currentDate))){
                OrderSales orderSales1=new OrderSales();
                orderSales1.setOrderDate(orderList.get(i).getOrderDate());
                orderSales1.setOrderSales(orderList.get(i).getTotalPrice());
                orderSalesList.add(orderSales1);
            }
        }*/
        return CommonResponse.createForSuccess("获取所有order成功",orderVOList);
    }



}
