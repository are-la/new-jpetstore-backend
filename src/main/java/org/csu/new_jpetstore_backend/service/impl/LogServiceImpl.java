package org.csu.new_jpetstore_backend.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.csu.new_jpetstore_backend.common.CommonResponse;
import org.csu.new_jpetstore_backend.entity.Cart;
import org.csu.new_jpetstore_backend.entity.Log;
import org.csu.new_jpetstore_backend.persistence.LogMapper;
import org.csu.new_jpetstore_backend.service.LogService;
import org.csu.new_jpetstore_backend.vo.CartVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Service
public class LogServiceImpl implements LogService {
    @Autowired
    private LogMapper logMapper;
    @Autowired
    private Log log;
    @Override
    public void addLog(String userid,String action) {
        Date date=new Date();
        SimpleDateFormat s1 = new SimpleDateFormat("YYYY-MM-dd HH:MM:ss");
        s1.format(date);
        log=new Log();
        log.setAction(action);
        log.setDate(date.toString());
        log.setUserid(userid);
        int i=logMapper.insert(log);
        System.out.println("i:"+i);

    }
    public CommonResponse<List<Log>> getTricks(String userid)
    {
        QueryWrapper<Log>logQueryWrapper=new QueryWrapper<>();
        logQueryWrapper.eq("userid",userid);
        List<Log> logList = logMapper.selectList(logQueryWrapper);
        return CommonResponse.createForSuccess("获取所有Log成功",logList);
    }
}