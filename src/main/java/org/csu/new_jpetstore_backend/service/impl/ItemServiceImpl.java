package org.csu.new_jpetstore_backend.service.impl;

import org.csu.new_jpetstore_backend.common.CommonResponse;
import org.csu.new_jpetstore_backend.entity.Item;
import org.csu.new_jpetstore_backend.persistence.ItemMapper;
import org.csu.new_jpetstore_backend.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("ItemService")
public class ItemServiceImpl implements ItemService {

    @Autowired
    ItemMapper itemMapper;

    @Override
    public CommonResponse<Object> GetAllItems() {
        List<Item> itemList = itemMapper.selectList(null);
        if(itemList == null){
            return CommonResponse.createForSuccessMessage("have not items!");
        }
        return CommonResponse.createForSuccess("getItem success!", itemList);
    }

    @Override
    public CommonResponse<Item> getItemById(String itemId) {
        Item  item=itemMapper.selectById(itemId);
        if(item == null){
            return CommonResponse.createForSuccessMessage("have not items!");
        }
        return CommonResponse.createForSuccess("getItem success!", item);

    }
}
