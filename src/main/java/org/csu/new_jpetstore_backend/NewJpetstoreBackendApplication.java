package org.csu.new_jpetstore_backend;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("org.csu.new_jpetstore_backend")
public class NewJpetstoreBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(NewJpetstoreBackendApplication.class, args);
    }

}
